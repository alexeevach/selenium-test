package edu.LoginPage;

import edu.Wrapper.Login;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.html.HTMLInputElement;

public class DataTest {

    public static final Login Login_Input = new Login(By.id ("_58_login"));
    public static final By Password_Input = By.id("_58_password");
    public static final By Submit_Button = By.xpath("//div[@class='button-holder ']//button");
    public static final By ErrorMessage = By.xpath("/html/body/div/div[2]/div/div[2]/div[1]/div/div/div/div/div[2]/form/fieldset/div[1]/div");
    public static final By Open_Menu = By.xpath("/html/body/div/div[1]/div/div");
    public static final By OpenHR_Startegy = By.xpath("/html/body/div/div[2]/div[1]/nav/ul/li[3]/a/div/div[3]/div/div");
    //public static final By List_HR_Strategy = By.xpath("/html/body/div/div[2]/div[1]/nav/ul/li[3]/ul");
    public static final By Button_AddCompetence = By.xpath("//div[@class = 'tc-orgpotential_wrapper-buttons margin-bottom-16']//button");
    public static final By Button_Add = By.xpath("//div[@class='ant-modal-footer']//button[2]");
    public static final By CheckCompetence = By.xpath("//div[@class='tree margin-top-16']//li[4]");
    public static final By Title = By.xpath("//div[@class='tc-orgpotential_wrapper-cards margin-top-16']//div[8]");
    public static final By Blocks = By.xpath("//div[@class='tc-orgpotential_wrapper-cards margin-top-16']");
    public static final By SelectFrom_List = By.xpath("//div[@class='tc-orgpotential_wrapper-filter common-container']/div[2]/div[3]/div[2]/div");
    public static final By Filters = By.xpath("//div[@class='tc-orgpotential_wrapper-filter common-container']");

    private WebDriver driver;
    private Object Actions;

    public DataTest(WebDriver driver)    {
        this.driver=driver;
    }

    public void typeLogin(String login) { Login_Input.sendKeys(login);
        }   //////

    public void typePassword(String password) {
        driver.findElement(Password_Input).sendKeys(password);
    }

    public void clickButton () {
        driver.findElement(Submit_Button).click();
    }

     public void login(String login, String password) {
        typeLogin(login);
        typePassword(password);
        clickButton();
     }

     public String getErrorMessage() {
        return driver.findElement(ErrorMessage).getText();
     }

     public void clickMenu () {
        driver.findElement(Open_Menu).click();
     }

     public void clickHR_Strategy() {
        driver.findElement(OpenHR_Startegy).click();
    }

    /* public String getList() { return driver.findElement(List_HR_Strategy).getText(); }
     тест в классе ненужных. в дальнейшем переделать */

    public void clickAddCompetence () {
        driver.findElement(Button_AddCompetence).click();
    }

    public void clickAdd () { driver.findElement(Button_Add).click(); }

    public void checkCompetence () { driver.findElement(CheckCompetence).click();}

    public void getCompetence () {driver.findElement(Title).getText();}

    public DataTest haveBlocks () {driver.findElement(Blocks); return this;}

    public DataTest haveFilter () {driver.findElement(Filters); return this;}

    public void changeValue() {
       driver.findElement(SelectFrom_List).click();
    }




}
