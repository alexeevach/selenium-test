package edu.Wrapper;

import edu.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {
    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;

    public Login (By locator) {this.locator = locator;}

    public void sendKeys(String login) {
        driver.findElement(locator).sendKeys(login);
    }
}
