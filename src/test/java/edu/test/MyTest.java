package edu.test;

import edu.LoginPage.DataTest;
import edu.driver.DriverSingleton;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import java.awt.*;

import static org.assertj.core.api.Assertions.assertThat;

public class MyTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = DriverSingleton.getInstance();

        }

    @Test
    public void login() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        //then
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest= new DataTest(driver);
        dataTest.login(login, password);
        }

    @Test
    public void ErrorMessage() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome1";
        //when
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest= new DataTest(driver);
        dataTest.login(login,password);
        //then
        assertThat(dataTest.getErrorMessage()).isEqualTo("Не удалось войти в систему.\nEmail или пароль введены неверно. Пожалуйста, попробуйте еще раз.");
    }

    @Test
    public void OpenMenu() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login,password);
        //then
        dataTest.clickMenu();
    }


    @Test
    public void OpenHR_Strategy() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login,password);
        //then
        dataTest.clickMenu();
        dataTest.clickHR_Strategy();
    }

    @Test
    public void ClickAddCompetence() throws InterruptedException {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login,password);
        //when
        dataTest.clickMenu();
        dataTest.clickHR_Strategy();
        Thread.sleep(2000);
        //then
        dataTest.clickAddCompetence();
    }

    @Test
    public void AddCompetence() throws InterruptedException {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login,password);
        //when
        dataTest.clickMenu();
        dataTest.clickHR_Strategy();
        Thread.sleep(2000);
        dataTest.clickAddCompetence();
        dataTest.checkCompetence();
        dataTest.clickAdd();
        //then
        dataTest.getCompetence();

    }

    @Test
    public void Blocks() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login, password);
        //when
        dataTest.clickMenu();
        dataTest.clickHR_Strategy();
        //then
        dataTest.haveBlocks();
    }

    @Test //есть вопрос
    public void List2() {
        //given
        String login = "zaitsev@tc.by";
        String password = "welcome";
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login(login, password);
        //when
        dataTest.clickMenu();
        dataTest.clickHR_Strategy();
        dataTest.haveFilter();
        dataTest.changeValue();
    }

    public void addFile() {
        driver.get("http://tc-stage04.k8s.iba/");
        DataTest dataTest = new DataTest(driver);
        dataTest.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage04.k8s.iba/c/portal/layout?p_l_id=20833&p_p_id=TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet&p_p_lifecycle=0&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_action=getPositionProfilePage&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_positionId=233&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_specId=265&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_isAdd=false&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_reserve=false");
        driver.findElement(By.id("//button[contains(@class, 'editSpecializationButton')]")).click();
        driver.findElement(By.id("add-attachment"));

    }

    @Test
    public void shouldAddFile () {
        String file = "C:\\Users\\user\\Desktop\\aliakseyeva\\selenium-tests-master\\src\\test\\resources\\example.txt";
        addFile();
        Robot robot=new Robot();

        robot.keyPress();






    }


    @AfterEach
    public void tearDown() {
        driver.close();
        DriverSingleton.destroyInstance();
    }
}




